import React from "react";
import { useMenu } from "../hooks/use-menu";

export default function MenuSlot({ Menu, menuURL, menuID }) {
  const menu = useMenu(menuURL, menuID);
  const tree = menu.tree ?? [];
  return <Menu tree={tree} />
}
